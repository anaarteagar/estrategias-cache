//Crear las variables de cache
const CACHE_DYNAMIC = 'dynamic-v1' //Para los archivos que se van a descargar
const CACHE_STATIC = 'static-v3'    //App shell
const CACHE_INMUTABLE = 'inmutable-v1'// CDN de terceros. LIBRERIAS



self.addEventListener('install', event => {

    const cahePromise = caches.open(CACHE_STATIC).then(cache => {

        return cache.addAll([

            '/',
            '/index.html',
            '/js/app.js',
            '/sw.js',
            'static/js/bundle.js',
            'favicon.ico',

        ])
    })
    const caheInmutable = caches.open(CACHE_INMUTABLE).then(cache => {

        return cache.addAll([

            'https://fonts.googleapis.com/css2?family=Inter:wght@300&family=Roboto:wght@100&display=swap'

        ])
    })
    event.waitUntil(Promise.all([cahePromise, caheInmutable]))
})


self.addEventListener('fetch', event => {
    //Cache with network fallback
    const respuesta = caches.match(event.request)
        .then(response => {
            if (response) return response
            //Si no existe el archivo lo descarga
            return fetch(event.request)
                .then(newResponse => {
                    //Agregar  al cache .clone
                    caches.open(CACHE_DYNAMIC)
                        .then(cache => {
                            cache.put(event.request, newResponse)
                        })
                    return newResponse.clone()
                })
        })

    event.respondWith(respuesta)
})
